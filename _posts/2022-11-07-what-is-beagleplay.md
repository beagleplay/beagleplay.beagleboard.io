# BeaglePlay

Your membership card for rebuilding the 21st century

* 1,000s of sensor, indicator, actuator, and connectivity add-ons
* Embedded camera and display add-ons
* Built in long-range wireless and wired connectivity
* Open hardware and software, ready for you to customize
