# Connect to BeaglePlay WiFi

* Look for SSID "BeaglePlay-*XXXX*"
    * *XXXX* changes between boards
    * Default password is `BeaglePlay`
* Browse to [beagleplay.local](http://beagleplay.local) or [192.168.8.1](http://192.168.8.1)
    * Use `wpa_cli` to connect to your local WiFi
